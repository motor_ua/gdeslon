<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale()], function() {
    Route::get('/', 'CatalogController@shops');
    Route::get('catalog/shops/{name?}', 'CatalogController@shops');
    Route::post('catalog/shops/{name?}', 'CatalogController@shops');
    Route::get('catalog/favorites/{name?}', 'CatalogController@favoritesShops');
    Route::post('catalog/favorites/{name?}', 'CatalogController@favoritesShops');
    Route::post('catalog/set_favorites', 'CatalogController@setFavorites');
    Route::get('catalog/iframe_shops', 'CatalogController@iframeShops');
    Route::get('catalog/coupons', 'CatalogController@coupons');
    Route::get('catalog/offers/{offer_category_id?}/{shop_id?}', 'CatalogController@offers');
    Route::get('/statistics', 'StatisticsController@index');
    Route::get('/get_cookie', 'CatalogController@getCookie');
    Route::get('/set_cookie', 'CatalogController@setCookie');
});
