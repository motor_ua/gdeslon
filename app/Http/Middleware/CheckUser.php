<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Models;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::check()) {
            $sid = $request->get('sid');

            if ($sid) {
                if (! Auth::loginUsingId($sid)) {
                    $client = new Client();
                    try {
                        $responseBody = (string)$client->get('http://cash-club.org/API/OutService.asmx/agentInfo?id=' . $sid)->getBody();
                        if ($responseBody) {
                            $response = \GuzzleHttp\json_decode($responseBody)[0];
                            $user = new Models\User();
                            $user->id = $sid;
                            $user->name = $response->NAME;
                            $user->email = $response->EMAIL;
                            if ($user->save()) {
                                if (! Auth::loginUsingId($sid)) {
                                    return redirect('http://cash-club.ru/ru-RU/Login/Default.aspx?ReturnURL=/ru-RU/Main/Default.aspx');
                                }
                            } else {
                                return redirect('http://cash-club.ru/ru-RU/Login/Default.aspx?ReturnURL=/ru-RU/Main/Default.aspx');
                            }
                        }
                    } catch (ClientException $e) {
                        return redirect('http://cash-club.ru/ru-RU/Login/Default.aspx?ReturnURL=/ru-RU/Main/Default.aspx');
                    }
                }
            } else {
                return redirect('http://cash-club.ru/ru-RU/Login/Default.aspx?ReturnURL=/ru-RU/Main/Default.aspx');
            }
        }

        return $next($request);
    }
}
