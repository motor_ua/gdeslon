<?php

namespace App\Http\Controllers;

use App\Models as Models;
use GuzzleHttp;
use Illuminate\Http\Request;
use Auth;

class StatisticsController extends Controller
{
    public function index(Request $request)
    {
        $guzzle = new GuzzleHttp\Client();

        $response = $guzzle->request(
            'POST', 'https://www.gdeslon.ru/api/orders',
            [
                GuzzleHttp\RequestOptions::JSON => ['sub_id' => Auth::user()->id],
                GuzzleHttp\RequestOptions::AUTH => ['78337', 'jnzxkvhL-gnbkA3xoAmo']
            ]
        );

        if ($response->getStatusCode() == 200) {
            $results = GuzzleHttp\json_decode($response->getBody()->getContents());
        }

        return view('/statistics')->with([
            'results' => isset($results) ? $results : []
        ]);
    }
}