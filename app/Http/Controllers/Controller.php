<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use App\Models;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $_user;

    public function __construct()
    {
        $this->middleware('check.user');
    }

    public function getUser()
    {
        if ($this->_user) {
            $user = $this->_user;
        } else {
            $user = Models\User::find(Auth::user()->id);
        }

        return $user;
    }
}
