<?php

namespace App\Http\Controllers;

use App\Models as Models;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Database\Query\Builder;

class CatalogController extends Controller
{
    const SHOPS_PER_PAGE = 16;

    public function shops(Request $request, $name = null)
    {
        if ($name) {
            $shops = Models\Shop::where('logo_file_name', '!=', '""')
                ->whereRaw("gs_commission_mark REGEXP '^[0-9]*[.,][0-9]*%$'")
                ->orderBy('name')->search($name)->paginate(self::SHOPS_PER_PAGE);
        } else {
            $shops = Models\Shop::where('logo_file_name', '!=', '')
                ->whereRaw("gs_commission_mark REGEXP '^[0-9]*[.,][0-9]*%$'")
                ->orderBy('name')->paginate(self::SHOPS_PER_PAGE);
        }

        $favoriteShopIds = [];
        foreach ($this->getUser()->shops()->get() as $shop) {
            $favoriteShopIds[] = $shop->id;
        }

        return view('catalog/shops')->with([
            'shops' => $shops,
            'subId' => Auth::user()->id,
            'search' => $name ? $name : '',
            'favoriteShopIds' => $favoriteShopIds,
            'searchUrl' => 'catalog/shops'
        ]);
    }

    public function favoritesShops(Request $request, $name = null)
    {
        if ($name) {
            $shops = $this->getUser()->shops()->where('logo_file_name', '!=', '')->orderBy('name')->search($name)->paginate(self::SHOPS_PER_PAGE);
        } else {
            $shops = $this->getUser()->shops()->where('logo_file_name', '!=', '')->orderBy('name')->paginate(self::SHOPS_PER_PAGE);
        }

        $favoriteShopIds = [];
        foreach ($this->getUser()->shops()->get() as $shop) {
            $favoriteShopIds[] = $shop->id;
        }

        return view('catalog/shops')->with([
            'shops' => $shops,
            'subId' => Auth::user()->id,
            'search' => $name ? $name : '',
            'favoriteShopIds' => $favoriteShopIds,
            'searchUrl' => 'catalog/favorites'
        ]);
    }

    public function iframeShops(Request $request)
    {
        $shops = Models\Shop::where('logo_file_name', '!=', '')->orderBy('name')->paginate(8);

        return view('catalog/iframe_shops')->with([
            'shops' => $shops,
            'subId' => Auth::user()->id
        ]);
    }

    public function coupons(Request $request)
    {
        $coupons = Models\Coupon::orderBy('name')->paginate(20);

        return view('catalog/coupons')->with([
            'coupons' => $coupons,
            'subId' => Auth::user()->id
        ]);
    }

    public function offers(Request $request, $offerCategoryId = 0, $shopId = null)
    {
        $offerCategories = Models\OfferCategory::where('parent_id', $offerCategoryId)
            ->orderBy('name')
            ->get();

        $breadcrumbs = [];

        $currentOfferCategory = Models\OfferCategory::find($offerCategoryId);

        if ($currentOfferCategory) {
            $offerShops = $currentOfferCategory->getRepository()->getShops();

            if (! $shopId) {
                $offers = $currentOfferCategory->offers()->orderBy('name')->paginate(20);
            } else {
                $shop = Models\Shop::find($shopId);
                $offers = $shop->offers()->orderBy('name')->paginate(20);
            }

            foreach (explode(',', $currentOfferCategory->path) as $pathOfferCategoryId) {
                $pathOfferCategory = Models\OfferCategory::find($pathOfferCategoryId);
                $breadcrumbs[] = ['id' => $pathOfferCategory->id, 'name' => $pathOfferCategory->name, 'href' => $pathOfferCategory->href(), 'shop' => false];
            };
        } else {
            if (! $shopId) {
                $offerShops = Models\Shop::orderBy('name')->get();
                $offers = Models\Offer::orderBy('name')->paginate(20);
            } else {
                $shop = Models\Shop::find($shopId);
                $offers = $shop->offers()->orderBy('name')->paginate(20);
                $offerShops = [$shop];
            }

        }

        if (isset($shop) && $shop) {
            $breadcrumbs[] = ['id' => '', 'name' => $shop->name, 'href' => '', 'shop' => true];
        }

        return view('catalog/offers')->with([
            'offerCategories' => $offerCategories,
            'currentOfferCategory' => $currentOfferCategory,
            'offers' => $offers,
            'breadcrumbs' => $breadcrumbs,
            'offerShops' => $offerShops,
            'shop' => isset($shop) && $shop ? $shop : '',
            'subId' => Auth::user()->id
        ]);
    }

    public function getCookie(Request $request)
    {
        $value = $request->cookie('test_cookie');

        if ($value) {
            dd('From COOKIE: ' . $value);
        }
    }

    public function setCookie()
    {
        return response('')->withCookie(cookie('test_cookie', gmdate('d.m.Y H:i:s', time())));
    }

    public function setFavorites(Request $request)
    {
        $response = [];

        if ($shop = Models\Shop::find($request->get('id'))) {
            if ($this->getUser()->shops()->find($shop->id)) {
                $this->getUser()->shops()->detach($shop->id);
                $response['active'] = false;
            } else {
                $this->getUser()->shops()->attach($shop->id);
                $response['active'] = true;
            }
            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Магазин не найден';
        }

        echo json_encode($response);
    }
}
