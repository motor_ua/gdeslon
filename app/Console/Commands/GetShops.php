<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use XmlParser;
use App\Models;

class GetShops extends Command
{
    const XML_URL = 'https://www.gdeslon.ru/api/users/shops.xml?api_token=1a52bcb86474f33216038d488cde03c868712284';
    const JSON_SHOPS_URL = 'http://api.gdeslon.ru/merchants.json';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gdeslon:shops';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Shops';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xml = XmlParser::load(self::XML_URL);

        $xmlContent = $xml->getContent();

        foreach($xmlContent->shops->shop as $shopXmlElement) {
            $categoriesIds = [];

            if ($shopXmlElement->{'traffic-types'}->{'traffic-type'}) {
                foreach ($shopXmlElement->{'traffic-types'}->{'traffic-type'} as $trafficTypeXmlElement) {
                    $trafficTypeName = strtolower(strval($trafficTypeXmlElement->name));
                    if ($trafficTypeName == 'cashback') {
                        $cashback = strval($trafficTypeXmlElement->allowed) == 'yes';
                    }

                }
            }

            if ($shopXmlElement->categories->category) {
                foreach ($shopXmlElement->categories->category as $categoryXmlElement) {
                    $categoryId = (int)$categoryXmlElement->id;
                    if (!$category = Models\Category::find($categoryId)) {
                        $category = new Models\Category();

                        $category->id = $categoryId;
                        $category->name = strval($categoryXmlElement->name);

                        $category->save();
                    }

                    $categoriesIds[] = $category->id;
                }
            }

            if (! Models\Shop::find((int)$shopXmlElement->id)) {
                $shop = new Models\Shop();

                $shop->id = (int)$shopXmlElement->id;
                $shop->logo_file_name = strval($shopXmlElement->{'logo-file-name'});
                $shop->country = strval($shopXmlElement->country);
                $shop->gs_commission_mark = strval($shopXmlElement->{'gs-commission-mark'});
                $shop->affiliate_link = strval($shopXmlElement->{'affiliate-link'});
                $shop->description = self::_replace4byte(strval($shopXmlElement->description));
                $shop->name = strval($shopXmlElement->name);
                $shop->url = strval($shopXmlElement->url);
                $shop->is_green = strval($shopXmlElement->{'is-green'}) == 'true';

                if (isset($cashback)) {
                    $shop->cashback = $cashback;
                }

                $shop->save();

                if ($categoriesIds) {
                    $shop->categories()->attach($categoriesIds);
                }
            }
        }

        $jsonShopsText = file_get_contents(self::JSON_SHOPS_URL);
        if ($jsonShopsText) {
            $shops = json_decode($jsonShopsText, true);

            foreach ($shops as $shopData) {
                $shopId = (int)$shopData['_id'];
                if (! Models\Shop::find($shopId)) {
                    $shop = new Models\Shop();

                    $shop->id = $shopId;
                    $shop->logo_file_name = '';
                    $shop->country = '';
                    $shop->gs_commission_mark = '';
                    $shop->affiliate_link = '';
                    $shop->description = '';
                    $shop->name = isset($shopData['name']) ? strval($shopData['name']) : '';
                    $shop->url = '';
                    $shop->is_green = true;

                    $shop->save();
                }
            }
        }
    }

    private function _replace4byte($string) {
        return preg_replace('%(?:
          \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
        )%xs', '', $string);
    }
}
