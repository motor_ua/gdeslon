<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models;

class GetOfferCategories extends Command
{
    const JSON_CATEGORIES_URL = 'http://api.gdeslon.ru/gdeslon-categories.json';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gdeslon:offer_categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get offer categories';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jsonCategoriesText = file_get_contents(self::JSON_CATEGORIES_URL);
        if ($jsonCategoriesText) {
            $offerCategories = json_decode($jsonCategoriesText, true);

            foreach ($offerCategories as $offerCategoryData) {
                $offerCategoryId = (int)$offerCategoryData['_id'];
                if (! $offerCategory = Models\OfferCategory::find($offerCategoryId)) {
                    $offerCategory = new Models\OfferCategory();
                    $offerCategory->id = $offerCategoryId;
                }

                $offerCategory->name = strval($offerCategoryData['name']);
                $offerCategory->parent_id = (int)$offerCategoryData['parent_id'];
                $offerCategory->is_archived = strval($offerCategoryData['is_archived']) == 'true';

                $pathArray = [];
                foreach ($offerCategoryData['path'] as $key => $path) {
                    $pathArray[$key] = $path;
                }

                $offerCategory->path = implode(',', $pathArray);

                $offerCategory->save();
            }
        }
    }
}
