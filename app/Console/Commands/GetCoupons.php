<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use XmlParser;
use App\Models;

class GetCoupons extends Command
{
    const XML_URL = 'https://www.gdeslon.ru/api/coupons.xml?search%5Bkind%5D=4&search%5Bcoupon_type%5D%5B%5D=coupons&search%5Bcoupon_type%5D%5B%5D=promo&api_token=1a52bcb86474f33216038d488cde03c868712284';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gdeslon:coupons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Coupons';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xml = XmlParser::load(self::XML_URL);

        $xmlContent = $xml->getContent();

        foreach($xmlContent->{'coupon-categories'}->{'coupon-category'} as $couponCategoryXmlElement) {
            $couponCategoryId = (int)$couponCategoryXmlElement->id;
            if (! $couponCategory = Models\CouponCategory::find($couponCategoryId)) {
                $couponCategory = new Models\CouponCategory();

                $couponCategory->id = $couponCategoryId;
                $couponCategory->name = strval($couponCategoryXmlElement->name);

                $couponCategory->save();
            }
        }

        foreach($xmlContent->kinds->kind as $kindXmlElement) {
            $kindId = (int)$kindXmlElement->id;
            if (! $kind = Models\Kind::find($kindId)) {
                $kind = new Models\Kind();

                $kind->id = $kindId;
                $kind->name = strval($kindXmlElement->name);

                $kind->save();
            }
        }

        foreach($xmlContent->coupons->coupon as $couponXmlElement) {
            $couponCategoriesIds = [];
            if ($couponXmlElement->{'coupon-categories'}->{'coupon-category'}) {
                foreach ($couponXmlElement->{'coupon-categories'}->{'coupon-category'} as $couponCategoryXmlElement) {
                    $couponCategoryId = (int)$couponCategoryXmlElement->id;
                    if (! $couponCategory = Models\CouponCategory::find($couponCategoryId)) {
                        $couponCategory = new Models\CouponCategory();

                        $couponCategory->id = $couponCategoryId;
                        $couponCategory->name = strval($couponCategoryXmlElement->name);

                        $couponCategory->save();
                    }

                    $couponCategoriesIds[] = $couponCategory->id;
                }
            }

            $couponId = (int)$couponXmlElement->id;
            if (! $coupon = Models\Coupon::find($couponId)) {
                $coupon = new Models\Coupon();

                $coupon->id = $couponId;
                $coupon->code = strval($couponXmlElement->code);
                $coupon->url_with_code = strval($couponXmlElement->{'url-with-code'});
                $coupon->url = strval($couponXmlElement->url);
                $coupon->name = strval($couponXmlElement->name);
                $coupon->instruction = self::_replace4byte(strval($couponXmlElement->instruction));
                $coupon->description = self::_replace4byte(strval($couponXmlElement->description));
                $coupon->kind_id = (int)$couponXmlElement->kind;
                $coupon->shop_id = (int)$couponXmlElement->{'merchant-id'};
                $coupon->start_at = \Carbon\Carbon::createFromTimestampUTC(strtotime($couponXmlElement->{'start-at'}))->toDateTimeString();
                $coupon->finish_at = \Carbon\Carbon::createFromTimestampUTC(strtotime($couponXmlElement->{'finish-at'}))->toDateTimeString();

                $coupon->save();

                if ($couponCategoriesIds) {
                    $coupon->couponCategories()->attach($couponCategoriesIds);
                }
            }
        }
    }

    private function _replace4byte($string) {
        return preg_replace('%(?:
          \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
        )%xs', '', $string);
    }
}
