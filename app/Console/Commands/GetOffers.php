<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mockery\CountValidator\Exception;
use XmlParser;
use App\Models;
use PDOException;
use ErrorException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Laravie\Parser\InvalidContentException;

class GetOffers extends Command
{
    const XML_URL = 'http://api.gdeslon.ru/api/search.xml?l=100&_gs_at=1a52bcb86474f33216038d488cde03c868712284';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gdeslon:offers {page_from} {page_to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get offers';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pageFrom = $this->argument('page_from');
        $pageTo = $this->argument('page_to');

        try {
            for ($i = $pageFrom; $i <= $pageTo; $i++) {
                print_r($i . "\n");
                try {
                    $xml = XmlParser::load(self::XML_URL . '&p=' . $i);

                    $xmlContent = $xml->getContent();

                    foreach ($xmlContent->shop->offers->offer as $offerXmlElement) {
                        $attributes = $offerXmlElement->attributes();

                        $idOffer = strval($attributes['id']);
                        $idShop = (int)$attributes['merchant_id'];

                        $shop = Models\Shop::find($idShop);
                        $offerCategoryId = (int)$attributes['gs_category_id'];

                        if (!Models\Offer::find($idOffer) && $shop && $shop->cashback && $offerCategoryId != 0) {
                            $offer = new Models\Offer();

                            $offer->id = $idOffer;
                            $offer->gs_product_key = strval($attributes['gs_product_key']);
                            $offer->available = strval($attributes['available']) == 'true';
                            $offer->shop_id = $idShop;
                            $offer->article = (int)$attributes['article'];
                            $offer->offer_category_id = $offerCategoryId;
                            $offer->price = (int)$offerXmlElement->price;
                            $offer->charge = floatval($offerXmlElement->charge);
                            $offer->currency_id = strval($offerXmlElement->currencyId);
                            $offer->picture = strval($offerXmlElement->currencyId);

                            $originalPicture = strval($offerXmlElement->original_picture);

                            $offer->thumbnail = strval($offerXmlElement->thumbnail);
//                            if ($thumbnail) {
//                                try {
//                                    $offer->thumbnail = base64_encode(file_get_contents($thumbnail));
//                                } catch (ErrorException $errorException) {
//                                    if ($originalPicture) {
//                                        try {
//                                            $offer->thumbnail = base64_encode(file_get_contents($originalPicture));
//                                        } catch (ErrorException $errorException) {
//
//                                        } catch (FatalErrorException $fatalErrorException) {
//
//                                        }
//                                    }
//                                }
//                            }

                            $offer->name = strval($offerXmlElement->name);
                            $offer->description = self::_replace4byte(strval($offerXmlElement->description));
                            $offer->vendor = strval($offerXmlElement->vendor);
                            $offer->model = strval($offerXmlElement->model);
                            $offer->url = strval($offerXmlElement->url);
                            $offer->destination_url_do_not_send_traffic = strval($offerXmlElement->{'destination-url-do-not-send-traffic'});
                            $offer->original_picture = $originalPicture;

                            try {
                                $offer->save();
                                print_r("." . "\n");
                            } catch (PDOException $pdoException) {
                                print_r("pdo_error" . "\n");
                                print_r($pdoException->getMessage() . "\n");
                            }
                        } else {
                            print_r("passed_shop_$idShop" . "\n");
                        }
                    }
                } catch (InvalidContentException $invalidContentException) {
                    print_r( "bad_xml_1_on_$i" . "_page\n");
                }
            }
        } catch (Exception $exception) {
            print_r("bad_xml_1_on_$i" . "_page\n");
        }
    }

    private function _replace4byte($string) {
        return preg_replace('%(?:
          \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
        )%xs', '', $string);
    }
}
