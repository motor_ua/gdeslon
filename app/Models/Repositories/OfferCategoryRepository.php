<?php

namespace App\Models\Repositories;

use App\Models\OfferCategory;

class OfferCategoryRepository {

    public $model;

    public function __construct(OfferCategory $offerCategory)
    {
        $this->model = $offerCategory;

        return $this;
    }

    public function getShops()
    {
        $shops = [];

        foreach ($this->model->offers as $offer) {
            if (! isset($shops[$offer->shop->id])) {
                $shops[$offer->shop->id] = $offer->shop;
            }
        }

        return $shops;
    }
}