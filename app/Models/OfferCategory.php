<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Repositories;

class OfferCategory extends Model {

    protected $fillable = ['name', 'parent_id', 'path', 'is_archived'];

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function href($shopId = null)
    {
        if ($shopId) {
            $url =  url('/catalog/offers/' . $this->id . '/' . $shopId);
        } else {
            $url =  url('/catalog/offers/' . $this->id);
        }

        return $url;
    }

    public function getRepository()
    {
        return new Repositories\OfferCategoryRepository($this);
    }
}