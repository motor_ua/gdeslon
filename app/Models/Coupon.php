<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model {

    protected $fillable = ['code', 'url_with_code', 'url', 'name', 'instruction', 'description', 'kind_id', 'shop_id', 'start_at', 'finish_at'];

    public function kind()
    {
        return $this->belongsTo(Kind::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function couponCategories()
    {
        return $this->belongsToMany(CouponCategory::class);
    }

    public function getUrlWithCode($subId)
    {
        return url($this->url_with_code . '&sub_id=' . $subId);
    }

}