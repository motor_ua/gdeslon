<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $fillable = ['name'];

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }
}