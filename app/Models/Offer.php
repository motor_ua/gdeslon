<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model {

    protected $fillable = ['gs_product_key', 'available', 'charge', 'shop_id', 'article', 'coffer_category_id', 'price', 'picture', 'thumbnail', 'name', 'description', 'vendor', 'model', 'url', 'destination_url_do_not_send_traffic', 'original_picture', 'currency_id'];

    public function category()
    {
        return $this->belongsTo(OfferCategory::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function thumbnail()
    {
        $thumbnail = $this->getAttribute('thumbnail');
        return strstr($thumbnail, 'http') ? $thumbnail : 'data:image/png;base64,' . $thumbnail;
    }

    public function getUrl($subId)
    {
        return url($this->url . '?sub_id=' . $subId);
    }
}