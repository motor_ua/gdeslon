<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Shop extends Model {

    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'shops.name' => 10,
            'shops.url' => 9,
            'shops.description' => 8
        ]
//        ,
//        'joins' => [
//            'posts' => ['users.id','posts.user_id'],
//        ],
    ];

    protected $fillable = ['logo_file_name', 'country', 'gs_commission_mark', 'affiliate_link', 'description', 'name', 'url', 'is_green', 'cashback'];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function href($offerCategory = null)
    {
        $offerCategory = $offerCategory ? $offerCategory->id : 0;

        $url = url('/catalog/offers/' . $offerCategory . '/' . $this->id);

        return $url;
    }

    public function getAffiliateLink($subId)
    {
        return url($this->affiliate_link . '&sub_id=' . $subId);
    }
}