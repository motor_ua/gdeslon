<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->string('id', 255);
            $table->string('gs_product_key', 255);
            $table->boolean('available');
            $table->double('charge');
            $table->integer('shop_id')->unsigned();
            $table->foreign('shop_id')->references('id')->on('shops');
            $table->string('article', 255);
            $table->integer('offer_category_id')->unsigned()->index();
            $table->foreign('offer_category_id')->references('id')->on('offer_categories');
            $table->integer('price')->unsigned();
            $table->string('picture', 255);
            $table->longText('thumbnail')->nullable();
            $table->string('name', 255);
            $table->longText('description');
            $table->string('vendor', 255);
            $table->string('model', 255);
            $table->string('url', 255);
            $table->longText('destination_url_do_not_send_traffic');
            $table->string('original_picture', 255);
            $table->string('currency_id', 255);
            $table->timestamps();
            $table->primary(['id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offers');
    }
}
