<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo_file_name', 255);
            $table->string('country', 255);
            $table->string('gs_commission_mark', 255);
            $table->string('affiliate_link', 255);
            $table->longText('description');
            $table->string('name', 255);
            $table->string('url', 255);
            $table->boolean('is_green');
            $table->boolean('cashback')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shops');
    }
}