<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOfferCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_categories', function(Blueprint $table) {
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('path', 255);
            $table->boolean('is_archived');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_categories', function(Blueprint $table) {
            $table->dropColumn(['parent_id', 'path', 'is_archived']);
        });
    }
}
