$(function($) {

  // slider banners
  if ( $('.slider-lk-banners-bottom').length) {
    $('.slider-lk-banners-bottom').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      autoplay: true,
      autoplaySpeed: 2000,
    });
  }

  $('.side-menu-link a').on('click', function() {
    $('.side-menu-slide').slideToggle('fast')
    return false
  })

  $('.side-tabs-nav a').on('click', function() {
    $(this).addClass('active').siblings('a').removeClass('active')
  })

  $('.slide-side-tabs a').on('click', function() {
    $('.side-tabs').slideToggle('fast')
    return false
  })

})

