@extends('layouts.iframe')

@section('content')
    @if ($shops)
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="flex flex-align-center flex-justify-space-between">
                    <div>Каталог магазинов</div>
                </div>
            </div>
            <div class="panel-body">
                <div class="list-magazin row flex">
                    @foreach ($shops as $key => $shop)
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="list-magazin-item">
                                <a href="" class="list-magazin-liked active"><span class="glyphicon glyphicon-heart"></span></a>
                                @if ($shop->logo_file_name)
                                    <a href="{{ $shop->getAffiliateLink($subId) }}" class="list-magazin-img" target="_blank"><img src="{{ $shop->logo_file_name }}" alt="{{ $shop->name }}"></a>
                                @endif
                                <div class="list-magazin-bottom">
                                    <a href="{{ $shop->getAffiliateLink($subId) }}" class="list-magazin-name" target="_blank">{{ $shop->name }}</a>
                                    @if ($shop->gs_commission_mark)
                                        <div class="list-magazin-sale">{{ $shop->gs_commission_mark }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="panel-footer text-center">
                {!! $shops->render() !!}
            </div>
        </div>
    @endif
@stop