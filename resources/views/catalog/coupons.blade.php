@extends('layouts.main')

@section('title', 'Купоны и промокоды')

@section('content')
    <h1>@yield('title')</h1>
    <div class="flex row">
        @if ($coupons)
            <div class="col-xs-12 col-md-12 col-lg-12 content">
                <div class="box-pagination flex flex-align-center flex-justify-end">
                    {!! $coupons->render() !!}
                </div>
                <div class="list-coupons">
                    @foreach ($coupons as $key => $coupon)
                        <div class="list-coupons-item flex">
                            <div class="col-xs-12 col-sm-4">
                                <p>
                                    @if ($coupon->shop->logo_file_name)
                                        @if ($coupon->url_with_code)
                                            <a href="{{ $coupon->getUrlWithCode($subId) }}" class="list-coupons-img">
                                                <img src="{{ $coupon->shop->logo_file_name }}" alt="{{ $coupon->shop->name }}" class="img-responsive">
                                            </a>
                                        @else
                                            <img src="{{ $coupon->shop->logo_file_name }}" alt="{{ $coupon->shop->name }}" class="img-responsive">
                                        @endif
                                    @else
                                        {{ $coupon->shop->name }}
                                    @endif
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <a href="#" class="list-coupons-title">@if ($coupon->instruction) {{ $coupon->instruction }} @else {{ $coupon->shop->name }} @endif</a>
                                <div class="list-coupons-des">
                                    @if ($coupon->shop->name)
                                        <div><b>Магазин:</b> {{ $coupon->shop->name }}</div>
                                    @endif
                                    @if ($coupon->description)
                                        <div><b>Условия акции:</b> {!! nl2br($coupon->description) !!}</div>
                                    @endif
                                    @if ($coupon->start_at)
                                        <div><b>Действует с:</b> {{ $coupon->start_at }}</div>
                                    @endif
                                    @if ($coupon->finish_at)
                                        <div><b>Действует до:</b> {{ $coupon->finish_at }}</div>
                                    @endif
                                </div>
                                @if ($coupon->url_with_code)
                                    <a href="{{ $coupon->getUrlWithCode($subId) }}" class="btn btn-primary">Перейти</a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="box-pagination flex flex-align-center flex-justify-end">
                    {!! $coupons->render() !!}
                </div>
            </div>
        @endif
    </div>
@stop