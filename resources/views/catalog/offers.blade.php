@extends('layouts.main')

@section('title', 'Товары')

@section('content')
    <h1>@yield('title')</h1>
    <div class="flex row">
        <div class="col-xs-12 col-md-4 col-lg-3 sidebar">
            <div class="slide-side-tabs hidden-md hidden-lg"><a href="#"><span>Каталог</span></a></div>
            <div class="side-tabs">
                <div class="side-tabs-nav flex">
                    @if (count($offerCategories) > 0)
                        <a href="#tab-00" class="active" data-toggle="tab"><span>Категории</span></a>
                    @endif
                    @if (count($offerShops) > 0)
                        <a href="#tab-01" @if (count($offerCategories) == 0) class="active" @endif data-toggle="tab"><span>Магазины</span></a>
                    @endif
                </div>
                <div class="side-tabs-block tab-content">
                    @if (count($offerCategories) > 0)
                        <div class="tab-pane fade in active" id="tab-00">
                            <ul class="side-tabs-menu">
                                @foreach($offerCategories as $offerCategory)
                                    @if ($offerCategory->offers()->count() > 0)
                                        <li><a class="@if ($currentOfferCategory && $currentOfferCategory->id == $offerCategory->id) active @endif" href="{{ $offerCategory->href() }}">{{ $offerCategory->name }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (count($offerShops) > 0)
                        <div class="tab-pane fade @if (count($offerCategories) == 0) in active @endif" id="tab-01">
                            <ul class="side-tabs-menu">
                                @foreach($offerShops as $offerShop)
                                    @if ($offerShop->offers()->count() > 0)
                                        <li><a href="{{ $offerShop->href($currentOfferCategory) }}" class="@if ($shop && $shop->id == $offerShop->id) active @endif">{{ $offerShop->name }}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @if ($offers)
            <div class="col-xs-12 col-md-8 col-lg-9 content">
                @if ($offers->total() > $offers->perPage() || $currentOfferCategory)
                <div class="box-pagination flex flex-align-center flex-justify-end">
                    <div class="row">
                        @if ($currentOfferCategory)
                                <div class="col-md-12 text-right">
                                    <ol class="breadcrumb">
                                        <li><a href="{{ url('/catalog/offers') }}">Главная</a></li>
                                        @foreach ($breadcrumbs as $breadcrumb)
                                            @if ($currentOfferCategory->id == $breadcrumb['id'] && ! $shop)
                                                <li class="active">{{ $breadcrumb['name'] }}</li>
                                            @else
                                                @if ($breadcrumb['shop'])
                                                    <li>{{ $breadcrumb['name'] }}</li>
                                                @else
                                                    <li><a href="{{ $breadcrumb['href'] }}">{{ $breadcrumb['name'] }}</a></li>
                                                @endif
                                            @endif
                                        @endforeach
                                    </ol>
                                </div>
                        @endif
                        @if ($offers->total() > $offers->perPage())
                            <div class="col-md-12 text-right">
                                {!! $offers->render() !!}
                            </div>
                        @endif
                    </div>
                </div>
                @endif
                <div class="catalog">
                    @foreach ($offers as $offer)
                        <div class="catalog-item row flex">
                            <div class="col-xs-12 col-sm-3">
                                <p>
                                    @if ($offer->thumbnail)
                                        <a href="#"><img height="100px" src="{{ $offer->thumbnail() }}" alt="{{ $offer->name }} class="img-responsive"></a>
                                    @else
                                        {{ $offer->name }}
                                    @endif
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                @if ($offer->shop->logo_file_name)
                                    <div class="catalog-imgbrend">
                                        @if ($offer->url)
                                            <a href="{{ $offer->getUrl($subId) }}" class="catalog-title">{{ $offer->name }}</a>
                                            <a href="{{ $offer->getUrl($subId) }}"><img height="100px" src="{{ $offer->shop->logo_file_name }}" alt="{{ $offer->shop->name }}"></a>
                                        @else
                                            {{ $offer->name }}
                                            <img height="100px" src="{{ $offer->shop->logo_file_name }}" alt="{{ $offer->shop->name }}">
                                        @endif
                                    </div>
                                @else
                                    @if ($offer->url)
                                        <div class="catalog-imgbrend"><a href="{{ $offer->getUrl($subId) }}">{{ $offer->shop()->name }}</a></div>
                                    @else
                                        <div class="catalog-imgbrend">{{ $offer->shop()->name }}</div>
                                    @endif
                                @endif
                                <div class="catalog-des">
                                    @if ($offer->vendor)
                                        <div><b>Производитель:</b> {{ $offer->vendor }}</div>
                                    @endif
                                    @if ($offer->model)
                                        <div><b>Модель:</b> {{ $offer->model }}</div>
                                    @endif
                                </div>
                                @if ($offer->description)
                                <p>
                                    {!! nl2br($offer->description) !!}
                                </p>
                                @endif
                                @if ($offer->price)
                                    <div class="catalog-price"><span><b>Цена:</b> {{ $offer->price }} {{ $offer->currency_id }}</span></div>
                                @endif
                                @if ($offer->charge)
                                    <div class="catalog-price"><span><b>Вознаграждение:</b> {{ $offer->charge }} {{ $offer->currency_id }}</span></div>
                                @endif
                                @if ($offer->url)
                                    <a href="{{ $offer->getUrl($subId) }}" class="btn btn-primary">Перейти</a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                @if ($offers->total() > $offers->perPage())
                    <div class="box-pagination flex flex-align-center flex-justify-end">
                        {!! $offers->render() !!}
                    </div>
                @endif
            </div>
        @endif
    </div>
@stop