@extends('layouts.main')

@section('title', 'Магазины')

@section('content')
    <div class="row">
        <div class="flex flex-justify-space-between flex-align-center box-header-filter">
            <div class="col-xs-12 col-sm-6 col-md-8 flex flex-align-center">
                <div><a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), '/catalog/shops') }}" class="btn btn-default">Все магазины</a></div>
                <div><a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), '/catalog/favorites') }}" class="btn link-liked"><span><span class="glyphicon glyphicon-heart"></span></span> <u>Избранное</u></a></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="search">
                    {!! Form::open(['url' => $searchUrl, 'method' => 'post']) !!}
                        <div class="input-group">
                            <div class="input-group-btn"><button class="btn btn-default">Поиск</button></div>
                            <input type="text" class="form-control" id="search" value="{{ $search }}">
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @if ($shops)
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="flex flex-align-center flex-justify-space-between">
                    <div>Самые популярные</div>
                    <div><span class="lead"><span class="glyphicon glyphicon-star"></span></span></div>
                </div>
            </div>
            <div class="panel-body">
                @if (! $shops->isEmpty())
                    <div class="list-magazin row flex">
                    @foreach ($shops as $key => $shop)
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="list-magazin-item">
                                <a href="javascript:void(0)" data-id="{{ $shop->id }}" class="list-magazin-liked @if (in_array($shop->id, $favoriteShopIds)) active @endif"><span class="glyphicon glyphicon-heart"></span></a>
                                @if ($shop->logo_file_name)
                                    <a href="{{ $shop->getAffiliateLink($subId) }}" target="_blank" class="list-magazin-img"><img src="{{ $shop->logo_file_name }}" alt="{{ $shop->name }}"></a>
                                @endif
                                <div class="list-magazin-bottom">
                                    <a href="{{ $shop->getAffiliateLink($subId) }}" target="_blank" class="list-magazin-name">{{ $shop->name }}</a>
                                    @if ($shop->gs_commission_mark)
                                        <div class="list-magazin-sale">{{ $shop->gs_commission_mark }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>

{{--                        <div class="row m-b-30">
                            <div class="col-md-10">
                                <h2>{{ $shop->name }}</h2>
                                @if ($shop->description)
                                    <div class="m-b text-justify">{!! nl2br($shop->description) !!}</div>
                                @endif
                                @if ($shop->affiliate_link)
                                    <div><a href="{{ $shop->getAffiliateLink($subId) }}" class="btn btn-success" role="button">Перейти</a></div>
                                @endif
                            </div>
                        </div>--}}

                    @endforeach
                </div>
                @else
                    Магазинов не найдено
                @endif
            </div>
            @if ($shops->total() > $shops->perPage())
                <div class="panel-footer text-center">
                    {!! $shops->render() !!}
                </div>
            @endif
        </div>
    @endif
@stop
@section('scripts')
    <script>
        $('form').submit(function() {
            var formActionValue = $(this).attr('action');
            $(this).attr('action', formActionValue + '/' + $('#search').val());
            return true;
        });
        $('.list-magazin-liked').click(function() {
            var thisLink = $(this);
            $.ajax({
                method: 'post',
                url: '/catalog/set_favorites',
                data: {
                    'id': $(this).data('id'),
                    '_token': '{{ csrf_token() }}'
                }
            })
            .done(function(data) {
                var result = JSON.parse(data);
                if (result.status == 'success') {
                    if (result.active) {
                        thisLink.addClass('active');
                    } else {
                        thisLink.removeClass('active');
                    }
                }
            });
        });
    </script>
@stop