@extends('layouts.main')

@section('title', 'Статистика')

@section('content')
    <h1>@yield('title')</h1>
    @if ($results)
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID заказа</th>
                    <th>ID заказа в магазине</th>
                    <th>Магазин</th>
                    <th>Сумма заказа</th>
                    <th>Вознаграждение</th>
                    <th>Статус</th>
                    <th>Дата заказа</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($results as $results)
                    <tr>
                        <td>{{ $results->gdeslon_order_id }}</td>
                        <td>{{ $results->merchant_order_id }}</td>
                        <td>{{ $results->merchant_name }}</td>
                        <td>{{ $results->order_payment }} {{ strtoupper($results->currency) }}</td>
                        <td>{{ $results->partner_payment }} {{ strtoupper($results->currency) }}</td>
                        <td>
                            @switch($results->state)
                            @case('0')
                            новый
                            @break;
                            @endcase
                            @case('1')
                            отменен
                            @break;
                            @endcase
                            @case('2')
                            отложен
                            @break;
                            @endcase
                            @case('3')
                            подтвержден
                            @break;
                            @endcase
                            @case('4')
                            выплачен
                            @break;
                            @endcase
                            @case('5')
                            тестовый
                            @break;
                            @endcase
                            @endswitch
                        </td>
                        <td>{{ gmdate('d.m.Y H:i', strtotime($results->created_at)) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>Нет покупок</p>
    @endif
@stop