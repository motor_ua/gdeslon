<header>
    <div class="container">
        <div class="flex flex-align-center flex-justify-space-between header-topline">
            <div class="header-logo"><a href="{{ url('/') }}"><img src="{{ URL::asset('images/system/header-logo.png') }}" alt=""></a></div>
            <div class="flex flex-align-center header-hotmenu">
                <div class="header-user"><a href="http://cash-club.org/ru-RU/Self/EditAgentData/Default.aspx"><span class="hidden-xs">{{ Auth::user()->name }}</span></a></div>
                <div class="header-cash hidden-xs"><b>28 200</b></div>
                <div class="hidden-xs hidden-sm">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default">{{ trans('navbar.language') }}</button>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">{{ trans('navbar.language') }}</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li>
                                    <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode)}}">
                                        {{$properties['native']}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="header-exit"><a href="http://cash-club.org/SignOut.aspx"><span class="hidden-xs">{{ trans('navbar.exit') }}</span></a></div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse bs-navbar-collapse collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
	        <li><a href="http://cash-club.org/ru-RU/Main/Default.aspx">Главная</a></li>
                <li  ><a href="http://cash-club.org/ru-RU/News/Default.aspx">{{ trans('navbar.news') }}</a></li>
                <li class="dropdown">
                    <a href="#" class="active dropdown-toggle" data-toggle="dropdown">Выгодные покупки<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), '/catalog/shops') }}">{{ trans('navbar.shops') }}</a></li>
                      <li><a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), '/catalog/coupons') }}">{{ trans('navbar.coupons') }}</a></li>
                      <li><a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), '/catalog/offers') }}">{{ trans('navbar.offers') }}</a></li>
                      <li><a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), '/statistics') }}">{{ trans('navbar.statistics') }} </a></li>
                    </ul>
                </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Мои финансы <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://cash-club.org/ru-RU/account/View/Default.aspx">Детализация счета</a></li>
                            <li><a href="http://cash-club.org/ru-RU/account/online/Default.aspx">Купить абонемент</a></li>
                            <li><a href="http://cash-club.org/ru-RU/account/AccountFillUp/Default.aspx">Пополнить счет</a></li>
                            <li><a href="http://cash-club.org/ru-RU/account/online_statute/Default.aspx">Повысить статус</a></li>
                            <li><a href="http://cash-club.org/ru-RU/account/FundsOut/Default.aspx">Вывести средства</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Моя команда <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://cash-club.org/ru-RU/structure/View/Default.aspx">Лично приглашенные</a></li>
                            <li><a href="http://cash-club.org/ru-RU/structure/morpheus/Default.aspx">Моя структура</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Правила <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://cash-club.org/ru-RU/rules/club_Rules/Default.aspx">Правила клуба</a></li>
                            <li><a href="http://cash-club.org/ru-RU/rules/Return_policy/Default.aspx">Политика возврата</a></li>                            
                            <li><a href="http://cash-club.org/ru-RU/rules/584/Default.aspx">Терминология</a></li>
                            <li><a href="http://cash-club.org/ru-RU/582/585/Default.aspx">FAQ Клуба</a></li>
                            <li><a href="http://cash-club.org/ru-RU/582/586/Default.aspx">FAQ Выгодные покупки</a></li>                            
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">О нас <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://cash-club.org/ru-RU/about/treaties/Default.aspx">Договоры</a></li>
                            <li><a href="http://cash-club.org/ru-RU/MessageToCompany/Default.aspx">Обратная связь</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>