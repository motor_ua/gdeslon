<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap-theme.css') }}">
    <link type="application/javascript" src="{{ URL::asset('js/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/media.css') }}">
</head>
<body>
<div  >
    @yield('content')
</div>
<script src="{{ URL::asset('js/jquery-1.11.3.min.js') }}"></script>
<script src="{{ URL::asset('js/slick/slick.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/ammi.js') }}"></script>
</body>
</html>