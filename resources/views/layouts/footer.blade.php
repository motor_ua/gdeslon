<footer>
    <div class="container">
        <div class="flex flex-justify-space-between">
            <div class="flex">
                <div class="footer-logo"><a href="{{ url('/') }}"><img src="{{ URL::asset('images/system/footer-logo.png') }}" alt=""></a></div>
                <div class="footer-menu hidden-xs hidden-sm">
                    <img src="{{ URL::asset('images/system/icon-wallet-green.png') }}" alt="">
                    <div class="footer-menu-title">Управление</div>
                    <ul>
            <li><a href="http://cash-club.org/ru-RU/account/View/Default.aspx">Узнать баланс</a></li>
            <li><a href="http://cash-club.org/ru-RU/account/AccountFillUp/Default.aspx">Пополнить баланс</a></li>
            <li><a href="http://cash-club.org/ru-RU/account/FundsOut/Default.aspx">Вывести средства</a></li>
            <li><a href="http://cash-club.org/ru-RU/structure/morpheus/Default.aspx">Проверить структуру</a></li>
            <li><a href="http://cash-club.org/ru-RU/account/online/Default.aspx">Повысить статус</a></li>
                    </ul>
                </div>
                <div class="footer-menu hidden-xs hidden-sm">
                    <img src="{{ URL::asset('images/system/icon-info-green.png') }}" alt="">
                    <div class="footer-menu-title">Информация</div>
                    <ul>
            <li><a href="http://cash-club.org/ru-RU/rules/club_Rules/Default.aspx">Правила Клуба</a></li>
            <li><a href="http://cash-club.org/ru-RU/rules/584/Default.aspx">Терминология</a></li>
            <li><a href="http://cash-club.org/ru-RU/rules/585/Default.aspx">FAQ Клуба</a></li>
            <li><a href="http://cash-club.org/ru-RU/rules/586/Default.aspx">FAQ Выгодные покупки</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-box-phone">
                <p><a href="http://cash-club.org/ru-RU/about/MessageToCompany/Default.aspx">О нас</a></p>
                <p>Техподдержка <a href="mailto:info@cash-club.org">info@cash-club.org</a></p>
                <div class="footer-phone">8-800-5000-784</div>
            </div>
        </div>
    </div>
</footer>
