<?php

return [
    'shops' => 'Shops',
    'coupons' => 'Coupons',
    'offers' => 'Offers',
    'statistics' => 'Statistics',
    'exit' => 'Exit',
    'language' => 'Language',
    'news' => 'News'
];