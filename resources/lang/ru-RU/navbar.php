<?php

return [
    'shops' => 'Магазины',
    'coupons' => 'Скидки',
    'offers' => 'Товары',
    'statistics' => 'Статистика',
    'exit' => 'Выход',
    'language' => 'Язык',
    'news' => 'Новости',
];